# aa30zero_gui_slider.py
# slider version

import serial
import time
import sys
import math
import tkinter as tk
import matplotlib.pyplot as plt


def mystart(event):
    print("clicked at", event.x, event.y, ", v = ", v2.get())
    print(Slider0.get(), Slider1.get())
    measure(Slider0.get(), Slider1.get(), v2.get())


def myquit(event):
    print("quit at", event.x, event.y)
    sys.exit(0)


def measure(i0, i1, i2):
    f = open('touchstone.s1p', 'w')
    ser = serial.Serial('/dev/cu.usbmodem14701', 38400, timeout=1)
    print(ser.name)

    time.sleep(2)
    command_0 = b'ver' + b'\x0a'
    print(command_0)
    ser.write(command_0)

    time.sleep(1)
    command_1 = b'fq' + str(int(10.0 * i0)).encode('ASCII') + b'00000' + b'\x0a'
    print(command_1)
    ser.write(command_1)

    time.sleep(1)
    command_2 = b'sw' + str(int(10.0 * i1)).encode('ASCII') + b'00000' + b'\x0a'
    print(command_2)
    ser.write(command_2)

    for i in range(3):
        line = ser.readline()
        print(line)

    time.sleep(1)
    ser.write(b'frx' + str(ndiv_list[i2]).encode('ASCII') + b'\x0a')

    f.write('# MHz S RI R 50 \n')

    z0 = complex(50.0, 0.0)
    freq_list = []
    zr_list = []
    zi_list = []
    retloss_list = []
    vswr_list = []

    for i in range(int(ndiv_list[i2]) + 1):
        line = ser.readline().decode(encoding='utf-8').rstrip().split(',')
        freq = float(line[0])
        if freq < 0.5:
            continue
        z = complex(float(line[1]), float(line[2]))
        rho = (z - z0) / (z + z0)
        retloss = -20.0 * math.log10(abs(rho))
        vswr = (1 + abs(rho)) / (1 - abs(rho))
        freq_list.append(freq)
        zr_list.append(z.real)
        zi_list.append(z.imag)
        retloss_list.append(retloss)
        vswr_list.append(vswr)
        print(freq, z.real, z.imag, rho, retloss, vswr)
        f.write('{0} {1} {2} \n'.format(freq, rho.real, rho.imag))

    for i in range(1):
        line = ser.readline()
        print(line)

    ser.close()

    fig = plt.figure(1, figsize=(8, 18))
    ax0 = fig.add_subplot(311)
    ax0.plot(freq_list, zr_list, 'r-', label='Real{Z}', linewidth=3)
    ax0.plot(freq_list, zi_list, 'b-', label='Imag{Z}', linewidth=3)

    ax1 = fig.add_subplot(312)
    ax1.plot(freq_list, retloss_list, 'c-', label='Return Loss', linewidth=3)

    ax2 = fig.add_subplot(313)
    ax2.plot(freq_list, vswr_list, 'y-', label='VSWR', linewidth=3)

    global plt_count
    if plt_count == 0:
        ax0.set_xlabel('frequency [MHz]')
        ax0.set_ylabel('Z [ohm]')
        ax0.set_title("Impedance")
        ax0.grid()
        ax0.legend()

        ax1.set_xlabel('frequency [MHz]')
        ax1.set_ylabel('Return Loss [dB]')
        ax1.set_title("Return Loss")
        ax1.grid()
        ax1.legend()

        ax2.set_xlabel('frequency [MHz]')
        ax2.set_ylabel('VSWR')
        ax2.set_title("VSWR")
        ax2.grid()
        ax2.legend()

        plt.subplots_adjust(hspace=0.4)
    plt_count += 1
    plt.show()


plt_count = 0

ndiv_list = [10, 20, 50, 100, 200, 500, 1000, 2000]

root = tk.Tk()
root.title('AA-30 ZERO')
root.geometry('800x520')

Button = tk.Button(text='Start', width=20)
Button.bind("<Button-1>", mystart)
Button.place(x=50, y=30)

Button2 = tk.Button(text='Quit', width=20)
Button2.bind("<Button-1>", myquit)
Button2.place(x=450, y=30)

Slider0 = tk.Scale(root, from_=0.1, to=30.0,
                   orient=tk.HORIZONTAL, resolution=0.1,
                   digits=3, length=600, fg='#404000', bg='#80f0f0',
                   label='Center Frequency [MHz]')
Slider1 = tk.Scale(root, from_=0.1, to=30.0,
                   orient=tk.HORIZONTAL, resolution=0.2,
                   digits=3, length=600, fg='#404000', bg='#40f080',
                   label='Bandwidth [MHz]')

Slider0.place(x=100, y=100)
Slider1.place(x=100, y=200)

Slider0.set(7.0)
Slider1.set(2.0)

v2 = tk.IntVar()
v2.set(0)
tk.Label(root, text='ndiv').place(x=100, y=300)
for index, value in enumerate(ndiv_list):
    tk.Radiobutton(root,
                   text=value,
                   padx=20,
                   variable=v2,
                   value=index).place(x=300, y=300 + 25 * index)

root.mainloop()
sys.exit(0)
